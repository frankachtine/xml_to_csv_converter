import csv
import xml.etree.ElementTree as ET
import argparse

def convert_xml_to_csv(xml_file, config_file, output_file):
    # Charger la configuration depuis le fichier
    with open(config_file, 'r') as config_file:
        # Utiliser uniquement la première ligne du fichier de configuration
        config_lines = config_file.readlines()
        header_mapping = {line.split(',')[0]: line.split(',')[1].strip() for line in config_lines}

    # Analyser le fichier XML
    tree = ET.parse(xml_file)
    root = tree.getroot()

    # Créer le fichier CSV de sortie
    with open(output_file, 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)

        # Écrire l'en-tête CSV basé sur les noms de colonnes personnalisés du fichier de configuration
        header = list(header_mapping.keys())
        csv_writer.writerow(header)

        # Parcourir toutes les balises <a>
        for a_tag in root.findall('.//a'):
            # Récupérer les données pour chaque sous-balise spécifiée dans le fichier de configuration
            data = [a_tag.find(header_mapping[col]).text if a_tag.find(header_mapping[col]) is not None else '' for col in header]

            # Écrire une ligne dans le fichier CSV
            csv_writer.writerow(data)

    print("Conversion terminée avec succès.")

if __name__ == "__main__":
    # Configuration de l'analyseur d'arguments
    parser = argparse.ArgumentParser(description='Convertit un fichier XML en CSV en utilisant une configuration.')
    parser.add_argument('xml_file', help='Chemin du fichier XML en entrée')
    parser.add_argument('config_file', help='Chemin du fichier de configuration')
    parser.add_argument('output_file', help='Chemin du fichier CSV en sortie')
    args = parser.parse_args()

    # Vérifier si l'option -h a été spécifiée
    if args.xml_file == '-h' or args.config_file == '-h' or args.output_file == '-h':
        parser.print_help()
    else:
        # Convertir le XML en CSV en utilisant les arguments fournis
        convert_xml_to_csv(args.xml_file, args.config_file, args.output_file)
